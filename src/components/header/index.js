import React from 'react'
import Logo from '../../images/logo.png'

export default class Header extends React.Component {
  render() {
    return (
      <header className="app-header">
        <img className="app-header__logo" src={Logo} alt="logo" />
      </header>
    );
  }
}
