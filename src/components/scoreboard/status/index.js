import React from 'react'
import { string } from 'prop-types'
import WinImg from '../../../images/status-win.png'
import LoseImg from '../../../images/status-lose.png'
import TieImg from '../../../images/status-tie.png'

export default class Status extends React.Component {
  static propTypes = {
    status: string.isRequired
  }

  renderStatusImg() {
    let _status = this.props.status;
    if (_status === 'win') { return WinImg }
    else if (_status === 'lose') { return LoseImg }
    else { return TieImg }
  }

  render() {
    return (
      <div className="status">
        <img className="status__img" src={this.renderStatusImg()} alt="status-img"/>
      </div>
    )
  }
}
