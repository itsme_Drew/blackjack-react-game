import React from 'react'
import { number } from 'prop-types'

export default class GameScore extends React.Component {
  static propTypes = {
    score: number.isRequired
  }

  render() {
    return (
      <div className="game-score">hands won: {this.props.score}</div>
    );
  }
}
