import React from 'react'
import { string, number } from 'prop-types'
import Status from './status'
import GameScore from './game-score'

export default class Scoreboard extends React.Component {
  static propTypes = {
    handStatus: string.isRequired,
    score: number
  }

  constructor() {
    super();

    this.renderHandStatus = this.renderHandStatus.bind(this);
  }

  renderHandStatus() {
    let _handStatus = this.props.handStatus;

    if (_handStatus && _handStatus !== 'playing') {
      return <Status status={this.props.handStatus} />
    }
  }

  render() {
    return (
      <div className="scoreboard">
        {this.renderHandStatus()}
        <GameScore score={this.props.score} />
      </div>
    );
  }
}
