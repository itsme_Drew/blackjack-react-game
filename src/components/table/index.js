import React from 'react'
import { array } from 'prop-types'
import _ from 'lodash'
import Hand from '../hand'
import Controls from '../controls'
import Scoreboard from '../scoreboard'

export default class Table extends React.Component {
  static propTypes = {
    newDeck: array.isRequired
  }

  constructor() {
    super();

    this.state = {
      deck: [],
      handStatus: '',
      gameScore: 0,
      isGameOver: false,
      playerHandPoints: 0,
      playerHand: [],
      dealerHandPoints: '',
      dealerHand: []
    }

    this.shuffleDeck = this.shuffleDeck.bind(this);
    this.handleDeal = this.handleDeal.bind(this);
    this.handleScore = this.handleScore.bind(this);
    this.handleHit = this.handleHit.bind(this);
    this.handleStand = this.handleStand.bind(this);
    this.handlePlayAgain = this.handlePlayAgain.bind(this);
    this.dealHand = this.dealHand.bind(this);
  }

  shuffleDeck(deck) {
    return _.shuffle(deck);
  }

  componentWillMount() {
    this.setState({
      deck: this.shuffleDeck(this.props.newDeck)
    })
  }

  handleDeal() {
    let deck = this.state.deck;
    let playerHand = this.dealHand(deck);
    let dealerHand = this.dealHand(deck);

    this.setState({
      playerHand, playerHandPoints: this.handleScore(playerHand[0]),
      dealerHand, dealerHandPoints: this.handleScore(dealerHand[0]),
      deck,
      handStatus: 'playing'
    })
  }

  dealHand(deck) {
    return deck.splice(0, 2);
  }

  handleScore(hand) {
    let _hand = hand;
    let score = _.sum(_.map(_hand,'v'));

    if (score > 21) {
      let aceCount =  _.countBy(_hand, {v:11}).true;

      if (aceCount) {
        while (aceCount) {
          score = score - 10;
          aceCount = aceCount - 1;
        }
      }
    }

    return score;
  }

  hitMe(hand) {
    return hand.push(this.state.deck.pop())
  }

  handleHit() {
    let playerHand = this.state.playerHand;

    this.hitMe(playerHand);

    let playerHandPoints = this.handleScore(playerHand);

    if (playerHandPoints > 21) { this.setState({ handStatus: 'lose' })}

    this.setState({
      playerHand
    })
  }

  handleStand() {
    let dealerHand = this.state.dealerHand;
    let dealerHandPoints = this.handleScore(dealerHand);
    let playerHandPoints = this.handleScore(this.state.playerHand);
    let isGameOver = this.state.deck.length < 32;
    let handStatus = this.state.handStatus;

    while (dealerHandPoints < 17) {
      this.hitMe(dealerHand)
      dealerHandPoints = this.handleScore(dealerHand);
    }

    //todo - clean this up
    // maybe handle in the score function since that is called by hit and stand

    if (dealerHandPoints > 21 || playerHandPoints > dealerHandPoints) { handStatus = 'win' }
    else if (playerHandPoints === dealerHandPoints) { handStatus = 'tie' }
    else if (dealerHandPoints > playerHandPoints) { handStatus = 'lose' }
    else { return false }

    this.setState({
      dealerHand,
      dealerHandPoints,
      handStatus,
      isGameOver
    })
  }

  handlePlayAgain() {
    this.setState({
      deck: this.shuffleDeck(this.props.newDeck),
      isGameOver: false,
      gameScore: 0
    })
  }

  render() {
    const playerHand = this.state.playerHand;
    const dealerHand = this.state.dealerHand;
    const handStatus = this.state.handStatus;

    return (
      <div className="table">
        <Hand   hand={dealerHand}
                count={handStatus === 'playing' ? 1 : dealerHand.length}
        />
        <Scoreboard
                handStatus={handStatus}
                score={this.state.gameScore}
                />
        <Hand   hand={playerHand}
                count={playerHand.length}
                />
        <Controls {...this.state}
                onDeal={this.handleDeal}
                onHit={this.handleHit}
                onStand={this.handleStand}
                onPlayAgain={this.handlePlayAgain}
                />
      </div>
    )
  }
}
