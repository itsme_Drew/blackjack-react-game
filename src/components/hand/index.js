import React from 'react'
import _ from 'lodash'
import Card from './card'
import { array, number } from 'prop-types'

export default class Hand extends React.Component {
  static propTypes = {
    hand: array.isRequired,
    count: number
  }

  renderCards() {
    return _.map(this.props.hand, (card, key) => {
      if (key !== this.props.count) {
        return <Card face={card.f} key={key} />
      }
    })
  }

  render() {
    return (
      <div className="hand" >
        {this.renderCards()}
      </div>
    );
  }
}
