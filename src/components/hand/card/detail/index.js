import React from 'react'
import { string } from 'prop-types'
import classnames from 'classnames'

export default class CardDetail extends React.Component {
  static propTypes = {
    value: string.isRequired,
    suit: string.isRequired,
    position: string.isRequired
  }

  render() {
    return (
      <div className={classnames('card-detail--' + this.props.position)}>
        <span className="card-detail__value">{this.props.value}</span>
        <span className="card-detail__suit">{this.props.suit}</span>
      </div>
    )
  }
}
