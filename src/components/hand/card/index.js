import React from 'react'
import { string } from 'prop-types'
import classnames from 'classnames'
import CardDetail from './detail'

export default class Card extends React.Component {
  static propTypes = {
    face: string.isRequired
  }

  render() {
    const face = this.props.face;
    const valuePosition = face.length - 1;
    const value = face.slice(0, valuePosition);
    const suit = face.slice(valuePosition, face.length);
    const isRed = suit === "♥" || suit === "♦";
    const isBlack = suit === "♣" || suit === "♠";

    return (
      <div className={classnames('card', {red: isRed}, {black: isBlack} )}>
        <CardDetail value={value} suit={suit} position='top' />
        <div className="card__suit">{suit}</div>
        <CardDetail value={value} suit={suit} position='bottom' />
      </div>
    )
  }
}
