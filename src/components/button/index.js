import React from 'react'
import { func } from 'prop-types'

export default class Button extends React.Component {
  static propTypes = {
    onClick: func
  }

  render() {
    return (
      <button className="button" onClick={this.props.onClick}>{this.props.children}</button>
    );
  }
}
