import React from 'react'
import { func } from 'prop-types'
import Button from '../button'

export default class Controls extends React.Component {
  static propTypes = {
    onDeal: func.isRequired,
    onHit: func.isRequired,
    onStand: func.isRequired,
    onPlayAgain: func.isRequired
  }

  renderControls() {
    if (this.props.isGameOver) { return <Button onClick={this.props.onPlayAgain}>new game</Button> }

    if (this.props.handStatus === 'playing') {
      return (
        <div>
          <Button onClick={this.props.onHit}>hit</Button>
          <Button onClick={this.props.onStand}>stand</Button>
        </div>
      )
    } else {
      return <Button onClick={this.props.onDeal}>deal</Button>
    }
  }

  render() {
    return (
      <div className="controls">
        {this.renderControls()}
      </div>
    );
  }
}
