import React, { Component } from 'react'
import Header from './components/header'
import Table from './components/table'

class App extends Component {
  constructor() {
    super();

    this.state = {
      deck: [
            {v:11,f:"a♣"},{v:2,f:"2♣"},{v:3,f:"3♣"},{v:4,f:"4♣"},{v:5,f:"5♣"},{v:6,f:"6♣"},
            {v:7,f:"7♣"},{v:8,f:"8♣"},{v:9,f:"9♣"},{v:10,f:"10♣"},{v:10,f:"J♣"},{v:10,f:"Q♣"},{v:10,f:"K♣"},
            {v:11,f:"a♥"},{v:2,f:"2♥"},{v:3,f:"3♥"},{v:4,f:"4♥"},{v:5,f:"5♥"},{v:6,f:"6♥"},
            {v:7,f:"7♥"},{v:8,f:"8♥"},{v:9,f:"9♥"},{v:10,f:"10♥"},{v:10,f:"J♥"},{v:10,f:"Q♥"},{v:10,f:"K♥"},
            {v:11,f:"a♦"},{v:2,f:"2♦"},{v:3,f:"3♦"},{v:4,f:"4♦"},{v:5,f:"5♦"},{v:6,f:"6♦"},
            {v:7,f:"7♦"},{v:8,f:"8♦"},{v:9,f:"9♦"},{v:10,f:"10♦"},{v:10,f:"J♦"},{v:10,f:"Q♦"},{v:10,f:"K♦"},
            {v:11,f:"a♠"},{v:2,f:"2♠"},{v:3,f:"3♠"},{v:4,f:"4♠"},{v:5,f:"5♠"},{v:6,f:"6♠"},
            {v:7,f:"7♠"},{v:8,f:"8♠"},{v:9,f:"9♠"},{v:10,f:"10♠"},{v:10,f:"J♠"},{v:10,f:"Q♠"},{v:10,f:"K♠"}
      ]
    }
  }

  render() {
    return (
      <div className="App">
        <Header />
        <div className="container">
          <Table newDeck={this.state.deck} />
        </div>
      </div>
    );
  }
}

export default App;
